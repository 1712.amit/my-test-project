public with sharing class GetAllRecords {
    @AuraEnabled()
    public static List<Artical_Post__c> getAllArticalData(){
        return [SELECT Name,First_Name__c,	Last_Name__c,Email_Id__c,Artical_Post__c,Profile_Pic__c FROM Artical_Post__c];
    }
}