global class Batchs implements Database.Batchable<sObject> {
  global Database.QueryLocator start(Database.BatchableContext bc)
  {
      return Database.getQueryLocator('SELECT id,BillingStreet,BillingCity,BillingState,BillingPostalCode,(SELECT id,MailingStreet,MailingCity,MailingState,MailingPostalCode from Contacts) FROM Account where BillingState = \'TX\'');
  }
  global void execute(Database.BatchableContext bc,List<Account> acc)
  {
      List<Contact> contacts = new List<Contact>();
      for(Account account:acc)
      {
          for(Contact contact:account.contacts)
          {
              contact.MailingStreet = account.BillingStreet;
              contact.MailingCity = account.BillingCity;
              contact.MailingPostalCode = account.BillingPostalCode;
              contacts.add(contact);
          }
      }
      update contacts;
  }
    global void finish(Database.BatchableContext bc)
    {
        
    }
}