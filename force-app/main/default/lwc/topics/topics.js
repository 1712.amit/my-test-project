import { LightningElement, track, api,wire } from 'lwc';
import { createRecord, getRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getAllTopics from '@salesforce/apex/GetAllTopics.getAllTopics';
import { fireEvent,registerListener,unregisterAllListeners } from 'c/pubsub';
import {CurrentPageReference} from 'lightning/navigation';

export default class Topics extends LightningElement {
    @track statusColor = 'red-color';
    @track inputHeading;
    @track inputDescription;
    @track getData1;
    @track getDataTemp;
    @api getAllTopics; 

    @wire(CurrentPageReference)
    wiredPageRef(pageRef) {
      this.pageRef = pageRef;
      if(this.pageRef) registerListener('updateTopics', this.onUpdateHandler, this);
    }
  
    disconnectedCallback()
    { 
       unregisterAllListeners(this);
    }
  
    onUpdateHandler(payload)
    {
        this.getAllTopicList();
    }
    getAllTopicList() {
        getAllTopics().then(response => {
        this.getData1 = this.getDataTemp = response;
        }).catch(error => {
            console.log(error.message);
            this.getData1 = null;
        });
    }

    refreshData() {
        return refreshApex(this.getData1);
    }

    connectedCallback()
    {
        this.getAllTopicList();
    }
    handleSearchKeyInput(event)
    {
        const searchKey = event.target.value.toLowerCase();
        this.getData1 = this.getDataTemp.filter(session => session.Name.toLowerCase().includes(searchKey));
    }
    headingChangeHandler(event) {
        this.inputHeading = event.target.value;
    }
    descriptionChnageHandler(event) {
        this.inputDescription = event.target.value;
    }
    createTopic() {
        console.log(this.inputHeading + this.inputDescription);
        const fields = {
            'Name': this.inputHeading,    // Name, Phone,Website all key are casesensetive we cann't change the key name
            'Topic_Description__c': this.inputDescription
        };
        const recordInput = { apiName: 'Topics__c', fields };

        createRecord(recordInput).then(response => {   // we are creating the account here not insert any  value
            this.showToast('Create Topic', 'New Topic Created Success!', 'success');
            this.inputDescription = '';
            this.inputHeading = '';
            this.getAllTopicList();
            fireEvent(this.pageRef,'updateTopicListInFeature',{});
        }).catch(error => {
            this.showToast('Some Issue occured', error.body.message, 'error');
        });
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }
}