import { LightningElement, track } from 'lwc';

export default class Master extends LightningElement {
    @track status = true;
    @track myList;

    allHandler(event)
    {
        {
           // alert(event.detail.Id+this.status);
           this.myList = event.detail;
           this.status = false;
        }
    }
    callBack()
    {
        this.status = true;
    }
}